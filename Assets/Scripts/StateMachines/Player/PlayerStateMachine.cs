using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.StateMachines
{
    public class PlayerStateMachine : StateMachine
    {
        private void Start()
        {
            SwitchState(new PlayerTestState(this));
        }
    }
}