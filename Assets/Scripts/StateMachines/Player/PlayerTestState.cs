using System.Collections;
using System.Collections.Generic;
using TkrainDesigns.StateMachines;
using UnityEngine;

public class PlayerTestState : PlayerBaseState
{
    public PlayerTestState(PlayerStateMachine stateMachine) : base(stateMachine)
    {
    }

    private float delay = 10;
    
    public override void Enter()
    {
        Debug.Log("Enter");
    }

    public override void Tick(float deltaTime)
    {
        delay -= deltaTime;
        if (delay <= -0)
        {
            stateMachine.SwitchState(new PlayerTestState(stateMachine));
        }
        else
        {
            Debug.Log(delay);
        }
        
    }

    public override void Exit()
    {
        Debug.Log("Exit");
    }
}
